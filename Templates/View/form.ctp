<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo '<div class="btn-group">';
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
	echo '</div>';
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
$this->end();
?>
