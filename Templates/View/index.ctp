<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<div id="actions" class="btn-group" data-spy="affix" data-offset-top="30" data-offset-bottom="200">
		<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'add')); ?>
	</div>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Assunto) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'edit', $Assunto['Assunto']['id']), array('icon'=>'pencil')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'del', $Assunto['Assunto']['id']), array('icon'=>'trash','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			</div>
		</td>
		<td><?php echo $Assunto['Assunto']['nome']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>

