<?php
class AssuntosController extends SistemaAppController {
	
	public $uses = array('Sistema.Assunto');
	
	private function _related($id = 0) {
		//$Associadas = $this->Site->Associada->find('list',array('fields'=>array('id','nome')));
		//$this->set('Associadas',$Associadas);
	}
	
	public function filter() {
		
	}
	
	public function _save($id = null) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			if ($id) $data['Assunto']['id'] = $id;
			$this->Assunto->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function index() {
		
		$this->set('title_for_layout','Assuntos');
		$this->Assunto->Behaviors->attach('Containable');
		$this->Assunto->contain();
		
		$data = $this->Paginator->paginate('Assunto');
		$this->set('data', $data);
		$this->set('pagination', true);
		$this->set('pageHeader', 'Assuntos');
		$this->set('panelStyle', 'primary');

		
	}
	
	public function add() {
		
		$this->_save();
		$this->_related();
		$this->set('pageHeader', 'Novo Assunto');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Assunto');
		$this->render('form');
	}
	
	public function edit($id = null) {
		$this->_save($id);
		$this->_related($id);
		$this->set('pageHeader', 'Edita Assunto');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Assunto');
		$this->request->data = $this->Assunto->read(null, $id);
		$this->render('form');
	}
	
	public function del( $id = null ) {
		if ($this->request->isPost()) {
			try {
				$this->Assunto->delete($id);
				$this->Bootstrap->setFlash('Registro excluido com sucesso!.','info');
			} catch (Exception $e) {
				$this->Bootstrap->setFlash('Erro na exclusão do Registro! Em uso em relacionamento.','danger');
			}
		} else {
			$this->Bootstrap->setFlash('Erro na exclusão do Registro!','danger');
		}
		$this->redirect(array('action'=>'index'));
	}
}
