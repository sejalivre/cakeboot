<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<title><?php echo $Sistematem['name'];?> - <?php echo $this->fetch('title_for_layout');?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<?php echo $this->Html->script('Bootstrap.jquery-2.1.0.min.js'); ?>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <?php echo $this->Html->script('Bootstrap./bootstrap/js/bootstrap.min'); ?>
    <!-- DateRangerPicker -->
    <?php echo $this->Html->script('Bootstrap./js/moment.min'); ?>
    <?php echo $this->Html->script('Bootstrap./js/moment.pt-br'); ?>
    <?php echo $this->Html->script('Bootstrap./js/daterangepicker'); ?>
    <!-- CKEditor -->
    <?php echo $this->Html->script('Bootstrap./js/ckeditor/ckeditor'); ?>
	<!-- Local scripts -->
	<?php echo $this->Html->script('authbootstrap'); ?>
    <script>
		$(document).ready(function(){
			$('.ckeditor').each(function(){
				CKEDITOR.replace( $(this).attr('id'), {
					language: 'pt',
				} );
			});
	    });
    </script>
    <!-- Timer -->
	<script>
	timer_minutes = <?php echo $div_timer['timeout'];?>;
	thirth = parseInt(timer_minutes/3);
	timer_seconds = 0;
	timer_logout = '<?php echo $div_timer['logout'];?>';
	console.log(timer_logout);
	<?php if (AuthComponent::user('id')) { ?>
	function clockwork() {
		timer_minutes = parseInt(timer_minutes);
		timer_seconds = parseInt(timer_seconds);
		timer_seconds--;
		// Alterar Cor de Fundo do timer
		if (timer_minutes >= thirth*2) {
			color = 'label-success';
		} else if (timer_minutes >= thirth) {
			color = 'label-warning';
		} else {
			color = 'label-danger';
		}
		$('#div-timer').removeClass('label-success').removeClass('label-warning').addClass(color);
		if (timer_seconds < 0) {
			timer_seconds = 59;
			timer_minutes--;
		}
		if (timer_minutes < 10) timer_minutes = '0'+timer_minutes;
		if (timer_seconds < 10) timer_seconds = '0'+timer_seconds;
		
		if (timer_minutes == '00' && timer_seconds == '00') {
			location.href = timer_logout;
		} else {
			$('#div-timer').html(timer_minutes+':'+timer_seconds);
			timer_interval = setTimeout(clockwork, 1000);
		}
	}
	timer_interval = setTimeout(clockwork, 1000);
	<?php } ?>
	</script>
	<!-- TBGui -->
	<?php echo $this->Html->css('Bootstrap./bootstrap/vanilla-cream-css/style'); ?>
	<?php echo $this->Html->script('Bootstrap./bootstrap/vanilla-cream-css/js/custom'); ?>
    <!-- Bootstrap -->
	<?php //echo $this->Html->css('Bootstrap./bootstrap/css/lumen.min'); ?>
	<?php echo $this->Html->css('Bootstrap./css/daterangepicker-bs3'); ?>
	<?php echo $this->fetch('script'); ?>
    <?php echo $this->fetch('css'); ?>
	<?php echo $this->Html->script('Bootstrap.cakeboot'); ?>
	<?php echo $this->Html->css('Bootstrap.cakeboot'); ?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<?php echo $this->Element('navbar-top', array('usuario',$usuario)); ?>
    <div class="container">
    	<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<?php echo $this->element('sql_dump'); ?>
  </body>
</html>
