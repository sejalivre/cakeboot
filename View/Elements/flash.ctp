<div class="alert alert-<?php echo $style;?> alert-dismissable">
	<i class="fa fa-<?php echo $style;?>"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<?php echo h($message); ?>
</div>
