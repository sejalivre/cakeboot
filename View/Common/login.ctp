<div class="row">
	<div class="col-md-4"><h3><?php echo ($this->fetch('pageHeader'))?($this->fetch('pageHeader')):(''); ?></h3></div>
	<div class="col-md-8 clearfix">
		<div class="pull-right">
			<?php echo ($this->fetch('actions'))?($this->fetch('actions')):('');?>
		</div>
	</div>
</div>
<hr>

<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->fetch('table-body'); ?>
	</div>
</div>
