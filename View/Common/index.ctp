<div class="row">
	<div class="col-md-4"><h3><?php echo ($this->fetch('pageHeader'))?($this->fetch('pageHeader')):(''); ?></h3></div>
	<div class="col-md-8 clearfix">
		<div class="pull-right">
			<div id="actions" class="btn-group" data-spy="affix" data-offset-top="40" data-offset-bottom="200">
			<?php echo ($this->fetch('actions'))?($this->fetch('actions')):('');?>
			</div>
		</div>
	</div>
</div>

<?php echo $this->fetch('tab-pills');?>
<hr>

<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-12">
		<?php if (count($data) == 0) { ?>
		<p class="text-warning">Nenhum registro encontrado!</p>
		<?php } else { ?>
		<?php if (isset($pagination)) { ?>
		<div class="row">
			<div class="col-md-12"><?php echo $this->Bootstrap->paginator(); ?></div>
		</div>
		<?php } ?>
		<div class="panel panel-<?php echo $panelStyle;?>">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $this->fetch('pageHeader');?></h3>
			</div>
			<table class="table">
				<thead>
				<?php echo $this->fetch('table-tr'); ?>
				</thead>
				<tbody>
				<?php echo $this->fetch('table-body'); ?>
				</tbody>
			</table>
		</div>
		<?php if (isset($pagination)) { ?>
		<div class="row">
			<div class="col-md-12"><?php echo $this->Bootstrap->paginator(); ?></div>
		</div>
		<?php } ?>
		<?php } ?>
	</div>
</div>
